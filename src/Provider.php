<?php

namespace mikk150\sms\turbosms;

use mikk150\sms\BaseProvider;
use mikk150\sms\turbosms\exceptions\BadRequestException;
use mikk150\sms\turbosms\exceptions\TransportException;
use yii\base\InvalidConfigException;
use yii\di\Instance;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;

/**
 * Class Provider
 */
class Provider extends BaseProvider
{
    const UA_NUMBER_EXTRACTOR = '/^\+(?<number>380[\d]{9})$/';

    const SUCCESS_STATUSES = [
        'SUCCESS_MESSAGE_ACCEPTED',
        'SUCCESS_MESSAGE_SENT',
        'SUCCESS_MESSAGE_PARTIAL_ACCEPTED',
        'SUCCESS_MESSAGE_PARTIAL_SENT',
    ];

    const REQUIRED_DATA_STATUSES = [
        'REQUIRED_MESSAGE_SENDER',
        'REQUIRED_MESSAGE_TEXT',
        'REQUIRED_MESSAGE_RECIPIENT',
    ];

    /**
     * @var array|string|Client
     */
    public $httpClient = [
        'class' => Client::class,
        'baseUrl' => 'https://api.turbosms.ua',
    ];

    /**
     * Bearer token to auth to TurboSMS.
     * Token you get from TurboSMS at <https://turbosms.ua/route/show.html>
     *
     * @link https://turbosms.ua/route/show.html
     * @var  string Bearer token
     */
    public $bearerToken;

    /**
     * @inheritdoc
     */
    public $messageClass = Message::class;

    /**
     * {@inheritdoc}
     *
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!$this->bearerToken) {
            throw new InvalidConfigException('Missing required parameter "bearerToken" when instantiating "' . __CLASS__ . '".');
        }

        parent::init();
    }

    /**
     * @inheritdoc
     * @throws     InvalidConfigException
     */
    protected function sendMessage($message)
    {
        $response = $this->getClient()->createRequest()
            ->setMethod('POST')
            ->setFormat(Client::FORMAT_JSON)
            ->setUrl('message/send.json')
            ->setData($this->buildRequest($message))
            ->setHeaders([
                'Authorization' => 'Bearer ' . $this->bearerToken
            ])
            ->send();

        if ($response->getStatusCode() !== '200') {
            throw new TransportException($response);
        }

        if (!ArrayHelper::isIn(
            ArrayHelper::getValue($response->getData(), 'response_status'),
            ArrayHelper::merge(self::REQUIRED_DATA_STATUSES, self::SUCCESS_STATUSES)
        )) {
            throw new BadRequestException($response);
        }

        if (ArrayHelper::isIn(ArrayHelper::getValue($response->getData(), 'response_status'), self::SUCCESS_STATUSES)) {
            return true;
        }

        return false;
    }

    /**
     * @param Message $message the message to be sent
     *
     * @return void
     */
    private function buildRequest($message)
    {
        return [
            'sms' => [
                'sender' => $message->getFrom(),
                'text' => $message->getBody()
            ],
            'recipients' => array_map([$this, 'formatRecipient'], (array) $message->getTo())
        ];
    }

    private function formatRecipient($recipient)
    {
        if (preg_match(self::UA_NUMBER_EXTRACTOR, $recipient, $matches)) {
            return $matches['number'];
        }

        return $recipient;
    }

    private $_client;

    /**
     * @return Client
     */
    private function getClient()
    {
        if (!$this->_client) {
            $this->_client = Instance::ensure($this->httpClient, Client::class);
        }

        return $this->_client;
    }
}
