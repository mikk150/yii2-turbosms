<?php

namespace mikk150\sms\turbosms\exceptions;

use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\httpclient\Response;

class BadRequestException extends Exception
{
    const REQUIRED_TOKEN = 'Missing authentication token.';
    const REQUIRED_CONTENT = 'Request data missing.';
    const REQUIRED_AUTH = 'Authentication failed, not valid token.';
    const REQUIRED_ACTIVE_USER = 'The user is locked, working with the API is not possible until unlocked.';
    const REQUIRED_BALANCE = 'There are not enough credits on the balance sheet to create the mailing list.';
    const REQUIRED_MESSAGE_BUTTON = 'Missing or empty button parameters in the message when required.';
    const REQUIRED_MESSAGE_BUTTON_CAPTION = 'Missing or empty text parameter on the button in the message.';
    const REQUIRED_MESSAGE_BUTTON_ACTION = 'Missing or empty URL parameter where the message recipient will go when the button is pressed.';
    const INVALID_REQUEST = 'Invalid request, check its structure and data correctness.';
    const INVALID_TOKEN = 'Invalid authentication token.';
    const INVALID_MESSAGE_SENDER = 'Invalid message sender.';
    const INVALID_START_TIME = 'Invalid date of postponed message sending.';
    const INVALID_MESSAGE_TEXT = 'Invalid message text value. Returned if a non-string value is passed or the character encoding is not in the UTF-8 set .';
    const INVALID_PHONE = 'Invalid recipient number, the system could not recognize the recipient country and operator.';
    const INVALID_TTL = 'Invalid value for the ttl parameter, the value must be an integer and not a string.';
    const INVALID_MESSAGE_ID = 'Invalid value for the message_id parameter, invalid format.';
    const NOT_ALLOWED_MESSAGE_SENDER = 'Not a valid sender for the current user.';
    const NOT_ALLOWED_MESSAGE_SENDER_NOT_ACTIVE = 'The sender is allowed, but not activated at the moment (not paid for use in the current month, registration not completed, etc.).';
    const NOT_ALLOWED_MESSAGE_IMAGE = 'Invalid image file type.';
    const NOT_ALLOWED_START_TIME = 'Invalid date of postponed message sending (out of limits).';
    const NOT_ALLOWED_NUMBER_STOPLIST = 'The recipient\'s number is in the stoplist (for sms ) or in the ignorlist (for Viber ), sending is not possible.';
    const NOT_ALLOWED_RECIPIENTS_LIMIT = 'Invalid number of recipients.';
    const NOT_ALLOWED_RECIPIENT_COUNTRY = 'Invalid recipient country. The user has not activated the ability to send messages to recipients of this country. To activate this feature, please contact our customer support department.';
    const NOT_ALLOWED_RECIPIENT_DUPLICATE = 'The recipient is already in the mailing list, duplicates are ignored.';
    const NOT_ALLOWED_MESSAGE_BUTTON_TEXT_LENGTH = 'The text on the button is too long, no more than 30 characters are allowed.';
    const NOT_ALLOWED_MESSAGE_TTL = 'Invalid value for the ttl parameter (outside the set limits).';
    const NOT_ALLOWED_MESSAGE_TRANSACTION_CONTENT = 'Invalid content in a transactional message. In such messages, only text can be sent, and the button and images are prohibited.';
    const NOT_ALLOWED_MESSAGE_DATA = 'Some of the parameters have an invalid value, please contact our customer support department for details.';
    const NOT_ALLOWED_MESSAGE_TEXT = 'The text contains forbidden fragments.';
    const NOT_ALLOWED_MESSAGE_TEXT_LENGTH = 'The length of the message text has been exceeded.';
    const NOT_ALLOWED_MESSAGE_ID = 'The message data with the passed message_id is not available for the current user.';
    const NOT_ALLOWED_MESSAGE_TRANSACTION_SENDER = 'It is forbidden to send transactional messages from a common sender.';
    const FAILED_CONVERT_RESULT2JSON = 'Failed to convert result data to JSON format, please contact our customer support department immediately for details.';
    const FAILED_CONVERT_RESULT2XML = 'Failed to convert result data to XML format, please contact our customer support department immediately for details.';
    const FAILED_PARSE_BODY = 'The body of the request could not be recognized (bad format).';
    const FAILED_SMS_SEND = 'Failed to send SMS message.';
    const FAILED_VIBER_SEND = 'Failed to send Viber message.';
    const FAILED_SAVE_IMAGE = 'Failed to save image.';
    const FATAL_ERROR = 'Request execution error, contact support for details.';

    /**
     * Constructor.
     *
     * @param \yii\httpclient\Response $response response body
     * @param \Exception $previous The previous exception used for the exception chaining.
     */
    public function __construct(Response $response, Exception $previous = null)
    {
        $error = ArrayHelper::getValue($response->data, 'response_status', 'FATAL_ERROR');
        $message = 'Error.';
        if (defined(self::class . '::' . $error)) {
            $message = constant(self::class . '::' . $error);
        }

        parent::__construct($message, ArrayHelper::getValue($response->data, 'response_code'), $previous);
    }
}
