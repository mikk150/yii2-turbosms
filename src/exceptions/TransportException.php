<?php

namespace mikk150\sms\turbosms\exceptions;

use Exception;
use yii\httpclient\Exception as HttpclientException;

class TransportException extends HttpclientException
{
    /**
     * @var \yii\httpclient\Response HTTP response instance.
     */
    public $response;

    /**
     * Constructor.
     *
     * @param \yii\httpclient\Response $response response body
     * @param \Exception $previous The previous exception used for the exception chaining.
     */
    public function __construct($response, Exception $previous = null)
    {
        $this->response = $response;

        parent::__construct($response->getContent(), $response->getStatusCode(), $previous);
    }
}
