<?php

namespace mikk150\sms\turbosms;

use mikk150\sms\BaseMessage;
use yii\base\InvalidArgumentException;

/**
 * Class Message
 */
class Message extends BaseMessage
{
    const UA_E164_FORMAT_PATTERN = '/^\+380[\d]{9}$/';

    /**
     * @var string
     */
    private $_from;

    /**
     * @var string|array
     */
    private $_to;

    /**
     * @var string
     */
    private $_body;

    /**
     * @inheritdoc
     */
    public function getFrom()
    {
        return $this->_from;
    }

    /**
     * @inheritdoc
     */
    public function setFrom($from)
    {
        $this->_from = $from;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getBody()
    {
        return $this->_body;
    }

    /**
     * @inheritdoc
     */
    public function setBody($body)
    {
        $this->_body = $body;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getTo()
    {
        return $this->_to;
    }

    /**
     * @inheritdoc
     * @throws     InvalidArgumentException
     */
    public function setTo($to)
    {
        array_map(function ($phoneNumber) {
            if (!preg_match(self::UA_E164_FORMAT_PATTERN, $phoneNumber)) {
                throw new InvalidArgumentException('Use ITU-T E.164 format');
            }
        }, (array) $to);

        $this->_to = $to;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function toString()
    {
        return $this->_body;
    }
}
