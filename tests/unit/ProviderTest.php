<?php

namespace tests\unit;

use Codeception\Stub\Expected;
use Codeception\Test\Unit;
use mikk150\sms\turbosms\exceptions\BadRequestException;
use mikk150\sms\turbosms\exceptions\TransportException;
use mikk150\sms\turbosms\Provider;
use yii\base\InvalidConfigException;
use yii\httpclient\Client;
use yii\httpclient\Request;
use yii\httpclient\Transport;

class ProviderTest extends Unit
{
    public function testInitializingWithoutToken()
    {
        $this->expectException(InvalidConfigException::class);
        $this->expectExceptionMessage('Missing required parameter "bearerToken" when instantiating "' . Provider::class . '".');

        new Provider();
    }

    public function testCompose()
    {
        $provider = new Provider([
            'bearerToken' => 'testToken',
        ]);

        $message = $provider->compose('Hello world {first_name}', ['first_name' => 'test'])
            ->setFrom('Test sender')
            ->setTo('+380322333333');

        $this->assertEquals('Hello world test', $message->getBody());
        $this->assertEquals('Test sender', $message->getFrom());
        $this->assertEquals('+380322333333', $message->getTo());
    }

    public function testComposeRecieverBeingArray()
    {
        $provider = new Provider([
            'bearerToken' => 'testToken',
        ]);

        $message = $provider->compose('Hello world {first_name}', ['first_name' => 'test'])
            ->setFrom('Test sender')
            ->setTo(['+380322333333', '+380322333334']);

        $this->assertEquals('Hello world test', $message->getBody());
        $this->assertEquals('Test sender', $message->getFrom());
        $this->assertEquals(['+380322333333', '+380322333334'], $message->getTo());
    }

    public function testSending()
    {
        $provider = new Provider([
            'bearerToken' => 'testToken',
            'httpClient' => new Client([
                'baseUrl' => 'https://api.turbosms.ua',
                'transport' => $this->make(Transport::class, [
                    'send' => function (Request $request) {
                        $request->beforeSend();

                        $request->prepare();

                        $url = $request->getFullUrl();
                        $method = strtoupper($request->getMethod());
                        
                        $this->assertEquals('https://api.turbosms.ua/message/send.json', $url);
                        $this->assertEquals('POST', $method);
                        $this->assertEquals('{"sms":{"sender":"Test sender","text":"Hello world test"},"recipients":["380322333333","380322333333"]}', $request->getContent());
                        $this->assertEquals('Bearer testToken', $request->getHeaders()->get('Authorization'));

                        return $request->client->createResponse(
                            '{
                                "response_code": 802,
                                "response_status": "SUCCESS_MESSAGE_PARTIAL_ACCEPTED",
                                "response_result": [
                                    {
                                        "phone": "380322333333",
                                        "response_code": 406,
                                        "message_id": null,
                                        "response_status": "NOT_ALLOWED_RECIPIENT_COUNTRY"
                                    },
                                    {
                                        "phone": "380322333333",
                                        "response_code": 0,
                                        "message_id": "f83f8868-5e46-c6cf-e4fb-615e5a293754",
                                        "response_status": "OK"
                                    }
                                ]
                            }',
                            [
                                'HTTP/1.1 200 OK',
                                'content-type: application/json; charset=UTF-8',
                            ]
                        );
                    }
                ])
            ])
        ]);

        $message = $provider->compose('Hello world {first_name}', ['first_name' => 'test'])
            ->setFrom('Test sender')
            ->setTo(['+380322333333', '+380322333333']);

        $this->assertTrue($provider->send($message));
    }

    public function testSendingEmptyRecipient()
    {
        $provider = new Provider([
            'bearerToken' => 'testToken',
            'httpClient' => new Client([
                'baseUrl' => 'https://api.turbosms.ua',
                'transport' => $this->make(Transport::class, [
                    'send' => Expected::once(function (Request $request) {
                        $request->beforeSend();

                        $request->prepare();

                        $url = $request->getFullUrl();
                        $method = strtoupper($request->getMethod());

                        $this->assertEquals('https://api.turbosms.ua/message/send.json', $url);
                        $this->assertEquals('POST', $method);
                        $this->assertEquals('Bearer testToken', $request->getHeaders()->get('Authorization'));
                        $this->assertEquals('{"sms":{"sender":"Test sender","text":"Hello world test"},"recipients":[]}', $request->getContent());

                        return $request->client->createResponse(
                            '{
                                "response_code": 202,
                                "response_status": "REQUIRED_MESSAGE_RECIPIENT"
                            }',
                            [
                                'HTTP/1.1 200 OK',
                                'content-type: application/json; charset=UTF-8',
                            ]
                        );
                    })
                ])
            ])
        ]);

        $message = $provider->compose('Hello world {first_name}', ['first_name' => 'test'])
            ->setFrom('Test sender');

        $this->assertFalse($provider->send($message));
    }

    /**
     * @return void
     */
    public function testSendingInternalServerError()
    {
        $this->expectException(TransportException::class);

        $provider = new Provider([
            'bearerToken' => 'testToken',
            'httpClient' => new Client([
                'baseUrl' => 'https://api.turbosms.ua',
                'transport' => $this->make(Transport::class, [
                    'send' => Expected::once(function (Request $request) {
                        $request->beforeSend();

                        $request->prepare();

                        $url = $request->getFullUrl();
                        $method = strtoupper($request->getMethod());

                        $this->assertEquals('https://api.turbosms.ua/message/send.json', $url);
                        $this->assertEquals('POST', $method);
                        $this->assertEquals('Bearer testToken', $request->getHeaders()->get('Authorization'));
                        $this->assertEquals('{"sms":{"sender":"Test sender","text":"Hello world test"},"recipients":[]}', $request->getContent());

                        return $request->client->createResponse(
                            'Please hold on!!',
                            [
                                'HTTP/1.1 500 Internal server error',
                                'content-type: application/json; charset=UTF-8',
                            ]
                        );
                    })
                ])
            ])
        ]);

        $message = $provider->compose('Hello world {first_name}', ['first_name' => 'test'])
        ->setFrom('Test sender');

        $this->assertFalse($provider->send($message));
    }
    /**
     * @return void
     */
    public function testSendingFatalError()
    {
        $this->expectException(BadRequestException::class);
        $this->expectExceptionMessage('Request execution error, contact support for details.');
        $this->expectExceptionCode(999);

        $provider = new Provider([
            'bearerToken' => 'testToken',
            'httpClient' => new Client([
                'baseUrl' => 'https://api.turbosms.ua',
                'transport' => $this->make(Transport::class, [
                    'send' => Expected::once(function (Request $request) {
                        $request->beforeSend();

                        $request->prepare();

                        $url = $request->getFullUrl();
                        $method = strtoupper($request->getMethod());

                        $this->assertEquals('https://api.turbosms.ua/message/send.json', $url);
                        $this->assertEquals('POST', $method);
                        $this->assertEquals('Bearer testToken', $request->getHeaders()->get('Authorization'));
                        $this->assertEquals('{"sms":{"sender":"Test sender","text":"Hello world test"},"recipients":[]}', $request->getContent());

                        return $request->client->createResponse(
                            '{
                                "response_code": 999,
                                "response_status": "FATAL_ERROR"
                            }',
                            [
                                'HTTP/1.1 200 OK',
                                'content-type: application/json; charset=UTF-8',
                            ]
                        );
                    })
                ])
            ])
        ]);

        $message = $provider->compose('Hello world {first_name}', ['first_name' => 'test'])
        ->setFrom('Test sender');

        $this->assertFalse($provider->send($message));
    }
}
