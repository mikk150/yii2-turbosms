<?php

namespace tests\unit;

use mikk150\sms\turbosms\Message;
use yii\base\InvalidArgumentException;

/**
 * Class MessageTest
 */
class MessageTest extends \Codeception\Test\Unit
{
    public function testFrom()
    {
        $message = new Message;
        $message->setFrom('123456');
        $this->assertEquals('123456', $message->getFrom());
    }

    public function testToSingle()
    {
        $message = new Message;
        $message->setTo('+380322333333');
        $this->assertEquals('+380322333333', $message->getTo());
    }

    public function testToMultiple()
    {
        $message = new Message;
        $message->setTo(['+380322333333', '+380322333334']);
        $this->assertEquals(['+380322333333', '+380322333334'], $message->getTo());
    }

    public function testToNotE164Format()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Use ITU-T E.164 format');

        $message = new Message;
        $message->setTo('380322333333');
    }

    public function testToInArrayNotE164Format()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Use ITU-T E.164 format');

        $message = new Message;
        $message->setTo(['380322333333', '380322333334']);
    }

    public function testToInArrayOneElementNotE164Format()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Use ITU-T E.164 format');

        $message = new Message;
        $message->setTo(['+380322333333', '380322333334', '+380322333335']);
    }

    public function testBody()
    {
        $message = new Message;
        $message->setBody('123456');
        $this->assertEquals('123456', $message->getBody());
    }

    public function testToString()
    {
        $message = new Message;
        $message->setBody('123456');
        $this->assertEquals('123456', $message->toString());
    }
}
