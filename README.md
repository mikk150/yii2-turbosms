# Yii2 Turbosms

Yii2 wrapper for Geordian TurboSMS sms provider

## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```bash
$ composer require mikk150/yii2-sms-ua-turbosms "^1.0.0"
```

or add

```
"mikk150/yii2-sms-ua-turbosms": "~1.0.0"
```

to the ```require``` section of your `composer.json` file.

## Configuration

### Via application component
```php
'components' => [
    'sms' => [
        'class' => mikk150\sms\turbosms\Provider::class,
        'bearerToken' => '<your turboSMS bearer token>',
    ],
],
```
## Usage

```php
Yii::$app
    ->sms
    ->compose('Text to send', ['attribute' => 'value'])
    ->setTo('123412341234')
    ->setFrom('Sender') 
    ->send()
;
```
